export {
  Context as CatsContext,
  Provider as CatsContextProvider,
  Consumer as CatsContextConsumer
} from "./CatsContext";