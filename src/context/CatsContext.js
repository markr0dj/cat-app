import React, { createContext, useState } from "react";
import PropTypes from "prop-types";

export const Context = createContext({});

export const Provider = props => {

  // Initial values are obtained from the props
  const {
    breed: initialBreed,
    cats: initialCats,
    page: initialPage,
    children
  } = props;

  // Use State to keep the values
  const [breed, setBreed] = useState(initialBreed);
  const [cats, setCats] = useState(initialCats);
  const [page, setPage] = useState(initialPage);

  // Make the context object:
  const catsContext = {
    breed,
    setBreed,
    cats,
    setCats,
    page,
    setPage
  };

  // pass the value in provider and return
  return <Context.Provider value={catsContext}>{children}</Context.Provider>;
};

export const { Consumer } = Context;

Provider.propTypes = {
  breed: PropTypes.string,
  cats: PropTypes.array,
  page: PropTypes.number,
  children: PropTypes.node
};

Provider.defaultProps = {
  breed: '',
  cats: [],
  page: 0,
};
