/*
 * Axios Request Wrapper
 */

import axios from 'axios';
import { NotificationManager } from 'react-notifications';

// API url and key
const API_KEY = process.env.REACT_APP_API_KEY;
const API_URL = process.env.REACT_APP_API_URL;

// Create axios client and set defaults
const client = axios.create({
  baseURL: API_URL,
  headers: {
    'x-api-key': API_KEY
  }
});

// Axios wrapper with default success and error actions
const Api = (options) => {
  // Return response data
  const onSuccess = (response) => {
    return response.data;
  }

   // Show notification message on error
  const onError = () => {
    NotificationManager.error('Apologies but we could not load new cats for you at this time!. Miau!');
  }

  return client(options)
            .then(onSuccess)
            .catch(onError);
}

export default Api;