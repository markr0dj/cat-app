import React, { useState, useEffect, useContext } from 'react';
import ApiService from '../../services/Api';
import CatItem from '../../components/CatItem';
import LoadMoreCats from '../../components/LoadMoreCats';
import { CatsContext } from '../../context';
import { Row, Col, Spinner } from 'react-bootstrap';
import './CatList.scss';

const CatList = () => {
	// Initial states
	const [isLoading, setIsLoading] = useState(false);
	const [hasMore, setHasMore] = useState(false);

	// Get cats context
  	const { breed, page, setPage, cats, setCats } = useContext(CatsContext);

  	// Page limit
	const limit = 10;

	// Get cats
	useEffect(() => {
		// Prevent request with an empty breed value
		if(!breed) {
			return;
		}

		// Loading state
		setIsLoading(true);

		// More cats state
		setHasMore(true);

		// Request for cats
	    ApiService({
			method: 'get',
		  	url: '/images/search',
		  	params: { 
    			breed_id: breed,
    			page: page,
    			limit: limit,
    			order: 'asc'
	      	} 
		})
		.then((response) => {
			// Prevent null response from being assigned to state
			if(response) {

				// Check if there are more cats to load
				if(cats.length) {
					setCats(cats.concat(response));
				}
				else {
					setCats(response);
				}

				if(response.length < limit) {
	        		setHasMore(false);
	        	}
			}

	        // Finished loading state
	        setIsLoading(false);
		});

  	}, [breed, page]);	

	// Load more cats action
  	const loadMore = () => {
  		setPage(page + 1);
  	}

  	return (
  		<>	
  			<Row>
	  			{!isLoading && !cats.length && 
	  				<Col md={12}>
	  					<p className="nocats">No cats available</p>
	  				</Col>
	  			}

	  			{isLoading && !cats.length &&
	  				<div className="center"><Spinner animation="border" /></div>
	  			}
		
	  			{cats && cats.map(item => (      
			       <CatItem item={item} key={item.id}/>
		      	))}
	      	</Row>
	      	{!!cats.length && hasMore &&
	      		<LoadMoreCats loadMore={loadMore} isLoading={isLoading} />
	      	}
  		</>
  	);
}

export default CatList;