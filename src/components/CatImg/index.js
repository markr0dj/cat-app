import React from 'react';
import PropTypes from "prop-types";
import './CatImg.scss';

const CatImg = (props) => {
	const { url } = props;

  	return (
  		<>
    		<div className="img" style={{ background: `url(${url}) center center no-repeat` }}></div>
	    </>
  	);
}

CatImg.propTypes = {
	url: PropTypes.string
};

export default CatImg;