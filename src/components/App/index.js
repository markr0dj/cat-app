import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { NotificationContainer } from 'react-notifications';
import HomePage from '../../pages/Home';
import SinglePage from '../../pages/Single';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-notifications/lib/notifications.css';
import './App.scss';

function App() {
  return (
    <div className="main">
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/:id" component={SinglePage} />
        </Switch>
        <NotificationContainer />
      </BrowserRouter>
    </div>
  );
}

export default App;
