import React, { useState, useEffect, useContext } from 'react';
import ApiService from '../../services/Api';
import { CatsContext } from '../../context';
import { Form, Row, Col } from 'react-bootstrap';
import './SelectBreed.scss';

const SelectBreed = () => {
	// Initial states
	const [breeds, setBreeds] = useState([]);
	const [isLoading, setIsLoading] = useState(false);
	
	// Get cats context
  	const { breed, setBreed, setPage, setCats } = useContext(CatsContext);

	// Get cat breed list
	useEffect(async () => {
		const breedsCache = getBreedsCache();

		console.info(breedsCache);

		if(breedsCache && breedsCache.data.length) {
			setBreeds(breedsCache.data);
			return;
		}

		setIsLoading(true);

	    ApiService({
		  method: 'get',
		  url: '/breeds'
		})
		.then((response) => {
			setBreedsCache(response);
			setBreeds(response);
	        setIsLoading(false);
		});
  	}, []);	

	// Set state to dropdown value
  	const handleChange = e => {
  		e.preventDefault();

  		// Assign dropdown value to breed
		setBreed(e.target.value);

		// Clear catlist states
		setCats([]);
		setPage(0);
	}

	// Cache to localstorage
	const BREEDS_CACHE = "BREEDS_CACHE";
	const ONE_DAY = 1000 * 60 * 60 * 24;

	const currentTime = () => {
	    return new Date().getTime();
	}

	const getBreedsCache = () => {
		const now = currentTime();
		let hasExpired = false;

		// Set default object
		let cache = {
			data: [],
			expiry: currentTime() + ONE_DAY
		};

		// Get breeds in local storage
		const localData = localStorage.getItem(BREEDS_CACHE);

		// Check if cache has expired
		if(localData && now - localData.expiry > ONE_DAY) {
			localStorage.removeItem(BREEDS_CACHE);
			hasExpired = true;
		}

		// Check if cache is not yet expired and not empty
		if(!hasExpired && localData) {
			cache = JSON.parse(localData);
		}

		return cache;
	}

	const setBreedsCache = (value) => {
		let breedsCache = getBreedsCache();

		// Check if cache is empty
		if(!breedsCache.data.length) {

			// Set and store value
			breedsCache.data = value || breedsCache.data;
			breedsCache.expiry = currentTime() + ONE_DAY;

			localStorage.setItem(BREEDS_CACHE, JSON.stringify(breedsCache));
		}	
	} 

  	return (
  		<>
	  		<Row>
	  			<Col lg={3} md={3} sm={12}>
				    <Form id={'select-breed'}>
					  <Form.Group>
					    <Form.Label>Breed</Form.Label>
						    <Form.Control as="select" size="lg" custom disabled={isLoading} id="breed" onChange={handleChange} value={breed}>
						   		<option>Select cat breed</option>
						      	{breeds && breeds.map(item => (
							        <option key={item.id} value={item.id}>
							          {item.name}
							        </option>
						      	))}
						    </Form.Control>
					  </Form.Group>
					</Form>
				</Col>
			</Row>
		</>
  	);
}

export default SelectBreed;