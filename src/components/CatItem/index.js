import React from 'react';
import PropTypes from "prop-types";
import { Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import CatImg from '../CatImg';
import './CatItem.scss';

const CatItem = (props) => {
	const { item } = props;

  	return (
  		<>	
	       <Col lg={3} md={6} sm={6}>
		    	<Card className="cat-list-item">
		    		<CatImg url={item.url} />
		    		<Card.Body>
		       			<Link to={item.id} className="btn btn-primary">View Details</Link>
		       		</Card.Body>
		    	</Card>
	       </Col>
  		</>
  	);
}

CatItem.propTypes = {
  item: PropTypes.object
};

export default CatItem;