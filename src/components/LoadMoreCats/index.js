import React from 'react';
import PropTypes from "prop-types";
import { Row, Col, Button } from 'react-bootstrap';
import './LoadMoreCats.scss';

const LoadMoreCats = (props) => {
    const { isLoading, loadMore } = props;

    const handleClick = e => {
      e.preventDefault();
      loadMore();
    }

  	return (
  		<>
  			<Row>
  				<Col>
            <div className="load-more-button">
  					 <Button variant="success" onClick={handleClick}>{isLoading ? 'Loading Cats...' : 'Load More'}</Button>
            </div>
  				</Col>
  			</Row>
	    </>
  	);
}

LoadMoreCats.propTypes = {
  isLoading: PropTypes.bool,
  loadMore: PropTypes.func
};

export default LoadMoreCats;