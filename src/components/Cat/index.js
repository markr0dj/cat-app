import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from "prop-types";
import { Container, Row, Col, Card } from 'react-bootstrap';
import CatImg from '../CatImg';
import './Cat.scss';

const Cat = (props) => {
	const { cat } = props;

  	return (
  		<>
        <Container>
    			<Row>
    				<Col>
              <Card className="single-cat">
                <Card.Header>
                  <Link to={'/?selected=' + (cat && cat.breeds[0].id)} className="btn btn-primary">Back</Link>
                </Card.Header>
                <CatImg url={cat && cat.url} />
                <Card.Body>
                  <h4>Name: {cat && cat.breeds[0].name}</h4>
                  <h5>Origin: {cat && cat.breeds[0].origin}</h5>
                  <p>Temperament: {cat && cat.breeds[0].temperament}</p>
                  <p>{cat && cat.breeds[0].description}</p>
                </Card.Body>
              </Card>
    				</Col>
    			</Row>
        </Container>
	    </>
  	);
}

Cat.propTypes = {
  cat: PropTypes.object
};

export default Cat;