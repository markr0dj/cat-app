import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import Cat from '../components/Cat';
import ApiService from '../services/Api';

const SinglePage = () => {
	const { id } = useParams();
	const [cat, setCat] = useState(null);
	const [isLoading, setIsLoading] = useState(false);

	// Get cats
	useEffect(() => {
		if(!id) {
			return;
		}

		setIsLoading(true);

		ApiService({
			method: 'get',
			url: `/images/${id}`,
		})
		.then((response) => {
			setCat(response);
			setIsLoading(false);
		});

	},[id]);

	return (
		<>
			{!isLoading && <Cat cat={cat}/>}
		</>
	);
}

export default SinglePage