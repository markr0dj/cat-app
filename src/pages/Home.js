import React from 'react';
import { useLocation } from 'react-router-dom';
import SelectBreed from '../components/SelectBreed';
import CatList from '../components/CatList';
import { CatsContextProvider } from "../context";
import { Container, Row, Col } from 'react-bootstrap';

const HomePage = () => {
	const useQuery = () => {
  		return new URLSearchParams(useLocation().search);
	}

	// Check URL for breed params
	const selected = useQuery().get('selected');
	const breed = selected ? selected : '';

  	return (
  		<>
	  		<Container>
	  			<Row>
			  		<Col lg={12}>
			  			<h1>Cat Browser</h1>
			  		</Col>
	  			</Row>
		  		<CatsContextProvider breed={breed}>
				    <SelectBreed />
				    <CatList />		   
				</CatsContextProvider>
			</Container>
	    </>
  	);
}

export default HomePage;